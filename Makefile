all:
	@echo "Here we go. This branch should have the submodule; testing:"
	@if [ -r "./testsubmodule/README.md" ]; then \
		echo "Success: submodule is present"; 				\
	else										\
		echo "Error: submodule is missing!!!!!!!!!!";		\
	fi
